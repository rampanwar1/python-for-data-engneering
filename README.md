
# Python topics for Data Engineer.

Here is some most important Python topics for Data Engineering.
Which will be very helpfull to learn Data Engineering.






## Lessons Learned

We will be exploring the following topics during our Data Engineering journey. The list of topics is provided below. Please review and provide any corrections or suggestions.



## List topics

1. **Input/Output** 

2. **Command Line Arguments**

- *Using python library*

3. **Data Types**

- *String*

- *List*

- *tuple*

- *Set*

- *Dictionary*

4. **Conditional/Logical/Mathematical Operators**


5. **If-else/Netsted if-else**

6. **For & while Loop**

7. **Functions**

- *Kwargs (How to pass key value Arguments in Functions)*
- *Passing/Accessing Arguments and Functions definations implementation*
- *how to retun multiple values in singal go*

8. **Lamda Functions**

9. **Exception Handling**

10. **File Handling**

11. **Modules**

12. **Super important libraries**

- *re(stands for regex)*
- *subprocess*
- *datetime*
- *json*
- *csv*
- *requests*
- *argparse*
- *configparser*
- *Database Access library ( mysql,Postgresql,Mssql)*
- *Logging*
- *pandas*
- *numpy*





## These are important python topics to learn Data Engineering.
